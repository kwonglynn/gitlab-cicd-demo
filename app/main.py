from fastapi import FastAPI

app = FastAPI()


@app.get("/hello-world")
async def hello_word():
    return {"message": "Hello, world!"}
